<?php

namespace Drupal\block_entity_form\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;


/**
 * Provides a block for creating a new content entity.
 *
 * @Block(
 *   id = "block_entity_form",
 *   admin_label = @Translation("Create Entity Form"),
 *   category = @Translation("Create Entity Form")
 * )
 */
class BlockEntityFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Creates a EntityEditFormBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'entity_type' => '',
      'bundle' => '',
      'form_mode' => 'default',
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return $this->entityTypeManager
      ->getAccessControlHandler($this->configuration['entity_type'])
      ->createAccess($this->configuration['bundle'], $account, [], TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    // Get content entity types.
    /** @var \Drupal\Core\Entity\ContentEntityTypeInterface[] $content_entity_types */
    $content_entity_types = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type_definition) {
      if ($entity_type_definition->getGroup() == 'content') {
        $content_entity_types[$entity_type_id] = $entity_type_definition;
      }
    }
    $options = array();
    foreach ($content_entity_types as $type_key => $entity_type) {
      // Entities that do not declare a form class.
      // Exclude Comment entities as they have to be attached to another entity.
      if (!$entity_type->hasFormClasses() || $type_key == 'comment') {
        continue;
      }
      // Get all bundles for current entity type.
      $entity_type_bundles = $this->entityTypeBundleInfo->getBundleInfo($type_key);
      foreach ($entity_type_bundles as $bundle_key => $bundle_info) {
        // Personal contact form requires a user recipient to be specified.
        if ($bundle_key == 'personal' && $type_key == 'contact_message') {
          continue;
        }
        $options[(string) $entity_type->getLabel()][$type_key . '.' . $bundle_key] = $bundle_info['label'];
      }
    }
    $form['#prefix'] = '<div id="create-entity-block-form">';
    $form['#suffix'] = '</div>';
    $form['entity_type_bundle'] = array(
      '#title' => $this->t('Entity Type + Bundle'),
      '#type' => 'select',
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $this->configuration['entity_type'] . '.' . $this->configuration['bundle'],
      "#ajax" => [ //ajax settings
        'callback' => [$this,'updateFormModes'], //the callback function that will check the form and update checkbox
        'event' => 'change',  // DOM event to trigger on form load
        'wrapper' => 'entity-form-mode',  //target id of checkbox element container
        'method' => "html",
        'progress' => [ 
          'type' => 'throbber',
          'message' => t('Loading Form Modes...'),
        ],
      ],
    );
    //get form modes
    $form['form_mode'] = [
    	"#prefix" => '<div id="entity-form-mode">',
    	"#suffix" => '</div>',
		'#type' => 'select',
		'#options' => $this->entityDisplayRepository->getFormModeOptions($this->configuration['entity_type']),
		"#validated" => TRUE,
		'#title' => $this->t('Form mode'),
		'#default_value' => $this->configuration['form_mode'],
    ];
    return $form;
  }

  public function updateFormModes($form, FormStateInterface $form_state){
  	$form_values = $form_state->getValues();
  	$selected_entity_type_bundle = $form_values['settings']['entity_type_bundle'];
    $values = explode('.', $selected_entity_type_bundle);
    $options = $this->entityDisplayRepository->getFormModeOptions($values[0]);
    $form['settings']['form_mode']['#options'] = $options;
    return $form['settings']['form_mode'];
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $selected_entity_type_bundle = $form_state->getValue('entity_type_bundle');
    $values = explode('.', $selected_entity_type_bundle);
    $this->configuration['entity_type'] = $values[0];
    $this->configuration['bundle'] = $values[1];
    $this->configuration['form_mode'] = $form_state->getValue('form_mode');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $values = array();
    // Specify selected bundle if the entity has bundles.
    if ($this->entityTypeManager->getDefinition($this->configuration['entity_type'])->hasKey('bundle')) {
      $bundle_key = $this->entityTypeManager->getDefinition($this->configuration['entity_type'])->getKey('bundle');
      $values = array($bundle_key => $this->configuration['bundle']);
    }
    //create entity
    $entity = $this->entityTypeManager
      ->getStorage($this->configuration['entity_type'])
      ->create($values);
    //set owner
    if ($entity instanceof EntityOwnerInterface) {
      $entity->setOwnerId(\Drupal::currentUser()->id());
    }
    //create form object
    $formObject = \Drupal::entityTypeManager()
  		->getFormObject($this->configuration['entity_type'], $this->configuration['form_mode'])
  		->setEntity($entity);
    //build form
    $form = \Drupal::formBuilder()->getForm($formObject);
    return $form;
    //return \Drupal::service('entity.form_builder')->getForm($entity, $this->configuration['form_mode']);
  }
}
